#FROM portalino:latest
FROM docker.io/wordpress:latest
#USER root

MAINTAINER Andrea Pisani

#ADD app/2irg.tar /var/www/html/wp-content/themes/
#RUN tar xvf /var/www/html/wp-content/themes/2irg.tar
#RUN rm -rf /var/www/html/wp-content/themes/2irg.tar
#RUN chown -R www-data:www-data /var/www/html/wp-content/themes/

ENV WORDPRESS_DB_USER user
ENV WORDPRESS_DB_PASSWORD password
ENV WORDPRESS_DB_NAME db
ENV WORDPRESS_DB_HOST mysql.develop.svc.cluster.local

EXPOSE 80